//
//  ContentView.swift
//  Social_61160154
//
//  Created by informatics on 3/17/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            ZStack(alignment: .center){
                Image("bg_pokeball")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .padding(-50.0)
                VStack{
                    VStack {
                        HStack(){
                            Image("logo2")
                                .resizable()
                                .frame(width: 300.0, height: 100.0)
                        }
                    }
                    .padding(.top, 80.0)
                    .padding(.bottom, 30.0)
                    VStack {
                        HStack{
                            NavigationLink(destination: Login()){
                                Text("Login")
                                    .padding()
                                    .frame(width: 300.0, height: 50.0)
                                    .foregroundColor(.white)
                                    .background(Color.black)
                                    .font(/*@START_MENU_TOKEN@*/.title2/*@END_MENU_TOKEN@*/)
                            }
                        }
                        .padding(.bottom, 10.0)
                    }
                    VStack {
                        HStack{
                            NavigationLink(
                                destination: Register(),
                                label: {
                                    Text("Register")
                                        .padding()
                                        .frame(width: 300.0, height: 50.0)
                                        .foregroundColor(.black)
                                        .background(Color.white)
                                        .font(/*@START_MENU_TOKEN@*/.title2/*@END_MENU_TOKEN@*/)
                                    
                                })
                        }
                    }
                    .padding(.bottom, 350.0)
                }
                .padding(-50.0)
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct Login: View {
    @State var username: String = ""
    @State var password: String = ""
    var body: some View {
        ZStack(alignment: .center){
            Image("bg_login")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .padding(-50.0)
            VStack {
                VStack{
                    HStack(){
                        Image("logo2")
                            .resizable()
                            .frame(width: 300.0, height: 100.0)
                    }
                    HStack{
                        Text("TRAINER HUB")
                            .font(.system(size: 30))
                            .fontWeight(.heavy)
                            .foregroundColor(Color(red: 1.0, green: 0.808, blue: 0.191))
                            //                            .background(Color.black)
                            .padding(/*@START_MENU_TOKEN@*/.all, 10.0/*@END_MENU_TOKEN@*/)
                            .overlay(
                                RoundedRectangle(cornerRadius: 15)
                                    .stroke(style: StrokeStyle(lineWidth: 5, dash: [15.0]))
                            )
                    }
                }
                .padding(.bottom, 50.0)
                
                VStack{
                    HStack{
                        TextField("USERNAME", text: $username)
                            .padding()
                            .background(Color(hue: 0.0, saturation: 0.032, brightness: 0.896, opacity: 0.876))
                            .frame(width: 300.0, height: 50.0)
                            .cornerRadius(40.0)
                            .padding(.bottom, 20)
                    }
                    HStack{
                        SecureField("PASSWORD", text: $password)
                            .padding()
                            .background(Color(hue: 0.0, saturation: 0.032, brightness: 0.896, opacity: 0.876))
                            .frame(width: 300.0, height: 50.0)
                            .cornerRadius(40.0)
                            .padding(.bottom, 20)
                    }
                    HStack{
                        NavigationLink(
                            destination: Home(),
                            label: {
                                Text("L O G I N")
                                    .fontWeight(.bold)
                                    .padding()
                                    .font(.system(size: 25
                                    ))
                                    .frame(width: 300.0, height: 50.0)
                                    .background(Color(red: 1.0, green: 0.808, blue: 0.191))
                                    .foregroundColor(Color(red: 0.003, green: 0.002, blue: 0.392))
                                    .cornerRadius(40
                                    )
                                
                            })
                    }
                }
                .padding(.bottom, 150.0)
            }
            .padding(-50.0)
        }
    }
}
struct Register: View {
    @State var username: String = ""
    @State var email: String = ""
    @State var password: String = ""
    @State var confirm_password: String = ""
    
    var body: some View {
        ZStack(alignment: .center){
            Image("bg_register")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .padding(-50.0)
            VStack {
                VStack{
                    //                    HStack(){
                    //                        Image("logo2")
                    //                            .resizable()
                    //                            .frame(width: 300.0, height: 100.0)
                    //                    }
                    HStack{
                        Text("CREATE YOUR ACCOUT")
                            .font(.system(size: 30))
                            .fontWeight(.heavy)
                            .foregroundColor(Color(red: 0.007, green: 0.002, blue: 0.396, opacity: 0.8))
                            //                            .background(Color(red: 1.0, green: 0.808, blue: 0.191))
                            .padding(/*@START_MENU_TOKEN@*/.all, 10.0/*@END_MENU_TOKEN@*/)
                        //                            .overlay(
                        //                                RoundedRectangle(cornerRadius: 15)
                        //                                    .stroke(style: StrokeStyle(lineWidth: 5, dash: [15.0]))
                        //                            )
                    }
                }
                .padding(.top, 100.0)
                
                VStack{
                    HStack{
                        TextField("EMAIL", text: $email)
                            .padding()
                            .background(Color.white)
                            .frame(width: 300.0, height: 50.0)
                            .cornerRadius(40.0)
                            .padding(.bottom, 20)
                    }
                    HStack{
                        TextField("USERNAME", text: $username)
                            .padding()
                            .background(Color.white)
                            .frame(width: 300.0, height: 50.0)
                            .cornerRadius(40.0)
                            .padding(.bottom, 20)
                    }
                    HStack{
                        TextField("PASSWORD", text: $password)
                            .padding()
                            .background(Color.white)
                            .frame(width: 300.0, height: 50.0)
                            .cornerRadius(40.0)
                            .padding(.bottom, 20)
                    }
                    HStack{
                        TextField("CONFIRM  PASSWORD", text: $confirm_password)
                            .padding()
                            .background(Color.white)
                            .frame(width: 300.0, height: 50.0)
                            .cornerRadius(40.0)
                            .padding(.bottom, 20)
                    }
                    HStack{
                        //                        Button(action: {}, label: {
                        //                            Text("R E G I S T E R")
                        //                                .fontWeight(.bold)
                        //                                .padding()
                        //                                .font(.system(size: 25
                        //                                ))
                        //                                .frame(width: 300.0, height: 50.0)
                        //                                .background(Color(red: 1.0, green: 0.808, blue: 0.191))
                        //                                .foregroundColor(Color(red: 0.003, green: 0.002, blue: 0.392))
                        //                                .cornerRadius(40
                        //                                )
                        //
                        //
                        //                        })
                        NavigationLink(
                            destination: Login(),
                            label: {
                                Text("R E G I S T E R")
                                    .fontWeight(.bold)
                                    .padding()
                                    .font(.system(size: 25
                                    ))
                                    .frame(width: 300.0, height: 50.0)
                                    .background(Color(red: 1.0, green: 0.808, blue: 0.191))
                                    .foregroundColor(Color(red: 0.003, green: 0.002, blue: 0.392))
                                    .cornerRadius(40
                                    )
                            })
                    }
                }
                .padding(.bottom, 150.0)
                .padding(.top, 50.0)
            }
            .padding(-50.0)
        }
    }
}

struct Home: View {
    var body: some View {
            ScrollView{
                VStack{
                    HStack{
                        Image("person1")
                            .resizable()
                            .padding(0.0)
                            .frame(width: 70.0, height: 50)
                            .clipShape(Circle())
                            .shadow(radius: 10)
                            .overlay(Circle().stroke(Color.gray, lineWidth: 2))
                        Text("Satoshi")
                            .font(.system(size: 20))
                            .fontWeight(.semibold)
                            .padding(.trailing, 20.0)
                        Image(systemName: "envelope.fill")
                            .resizable()
                            .frame(width: 30.0, height: 20.0)
                            .foregroundColor(/*@START_MENU_TOKEN@*/Color(hue: 0.624, saturation: 1.0, brightness: 1.0)/*@END_MENU_TOKEN@*/)
                        
                    }
                    .padding(.leading, 70.0)
                    VStack {
                        HStack{
                            Image("025_eating")
                                .resizable()
                                .frame(width: 300.0, height: 200.0)
                                .padding()
                                .background(Color.black)
                        }
                        .padding(0.0)
                        VStack(alignment: .leading){
                            HStack(){
                                Text("นี่คือหน้าหลังจากบอกให้ลดน้ำหนัก55555แต่ก็ไม่ได้หยุดกินเลย...")
                                    .multilineTextAlignment(.leading)
                            }
                            .padding(.trailing, 50.0)
                        }
                    }
                    .padding(.leading, 250.0)
                }
                .padding(.trailing, 250.0)
                VStack{
                    HStack{
                        Image("person2")
                            .resizable()
                            .padding(0.0)
                            .frame(width: 70.0, height: 50)
                            .clipShape(Circle())
                            .shadow(radius: 10)
                            .overlay(Circle().stroke(Color.gray, lineWidth: 2))
                        Text("Chilli   ")
                            .font(.system(size: 20))
                            .fontWeight(.semibold)
                            .padding(.trailing, 20.0)
                        Image(systemName: "envelope.fill")
                            .resizable()
                            .frame(width: 30.0, height: 20.0)
                            .foregroundColor(/*@START_MENU_TOKEN@*/Color(hue: 0.624, saturation: 1.0, brightness: 1.0)/*@END_MENU_TOKEN@*/)
                        
                    }
                    .padding(.leading, 70.0)
                    VStack {
                        HStack{
                            Image("fam")
                                .resizable()
                                .frame(width: 300.0, height: 200.0)
                                .padding()
                                .background(Color.white)
                        }
                        .padding(0.0)
                        VStack(alignment: .leading){
                            HStack(){
                                Text("ถ่ายรูปรวม 1 ใน 3 ยิมที่โหดที่สุดในเมือง Striaton สะหน่อยย คิ้คิ้คิ้")
                                    .multilineTextAlignment(.leading)
                                    .font(.system(size: 16))
                            }
                            .padding(.trailing, 50.0)
                        }
                    }
                    .padding(.leading, 250.0)
                }
                .padding(.trailing, 250.0)
                VStack{
                    HStack{
                        Image("person3")
                            .resizable()
                            .padding(0.0)
                            .frame(width: 70.0, height: 50)
                            .clipShape(Circle())
                            .shadow(radius: 10)
                            .overlay(Circle().stroke(Color.gray, lineWidth: 2))
                        Text("Touko")
                            .font(.system(size: 20))
                            .fontWeight(.semibold)
                            .padding(.trailing, 20.0)
                        Image(systemName: "envelope.fill")
                            .resizable()
                            .frame(width: 30.0, height: 20.0)
                            .foregroundColor(/*@START_MENU_TOKEN@*/Color(hue: 0.624, saturation: 1.0, brightness: 1.0)/*@END_MENU_TOKEN@*/)
                        
                    }
                    .padding(.leading, 70.0)
                    VStack {
                        HStack{
                            Image("000")
                                .resizable()
                                .frame(width: 300.0, height: 200.0)
                                .padding()
                                .background(Color.white)
                        }
                        .padding(0.0)
                        VStack(alignment: .leading){
                            HStack(){
                                Text("โปเกม่อนตัวแรกมาแล้ววววว ;--;")
                                    .multilineTextAlignment(.leading)
                            }
                            .padding(.trailing, 50.0)
                        }
                    }
                    .padding(.leading, 250.0)
                }
                .padding(.trailing, 250.0)
                
                .padding(.top,10)
                .navigationTitle("Home")
                .toolbar {
                    ToolbarItemGroup(placement: .navigationBarTrailing) {
                        VStack{
                            HStack{
                                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                                    Image(systemName: "house")
                                        .resizable()
                                        .frame(width: 25.0, height: 20.0)
                                        .foregroundColor(.blue)
                                        .padding()
                                })
                                NavigationLink(
                                    destination: Main(),
                                    label: {
                                        Image(systemName: "book")
                                            .resizable()
                                            .frame(width: 25, height: 20)
                                            .foregroundColor(.black)
                                            .padding()
                                    })
                                    .padding(.leading, 2.5)
                                
                                NavigationLink(
                                    destination: Text("This page is unavailable"),
                                    label: {
                                        Image("icon_pokeball")
                                            .resizable()
                                            .frame(width: 40.0, height: 40.0)
                                            .padding()
                                    })
                                .padding(.horizontal, 5.0)
                                NavigationLink(
                                    destination: Text("This page is unavailable"),
                                    label: {
                                        Image(systemName: "envelope")
                                            .resizable()
                                            .frame(width: 25.0, height: 20.0)
                                            .foregroundColor(.black)
                                            .padding()
                                    })
                                .padding(.trailing, 2.5)
                                NavigationLink(
                                    destination: Text("This page is unavailable"),
                                    label: {
                                        Image(systemName: "person")
                                            .resizable()
                                            .frame(width: 25.0, height: 20.0)
                                            .foregroundColor(.black)
                                            .padding()
                                    })
                            }
                        }
                        .padding(.bottom, 5.0)
                    }
                }
            }
    }
}

struct Main: View {
    
    private func getScale(proxy: GeometryProxy) -> CGFloat {
        var scale: CGFloat = 1.0
        
        let x = proxy.frame(in: .global).minX
        
        let diff = x - 32
        if diff < 100 {
            scale = 1 + diff / 500
        }
        return scale
    }
    var body: some View {
        ScrollView{
            ScrollView(.horizontal){
                HStack(spacing: 16){
                    GeometryReader {
                        proxy in
                        VStack{
                            let scale = getScale(proxy: proxy)
                            Image("news_pkMaster")
                                .resizable()
                                .clipped()
                                .cornerRadius(15)
                                .shadow(radius: 5)
                                .scaleEffect(CGSize(width: scale, height: scale))
                        }
                    }
                    .frame(width: 380,height:220.0)
                    
                    
                }.padding(18)
            }
            VStack{
                HStack{
                    Image("news_newPk")
                        .resizable()
                        .padding(/*@START_MENU_TOKEN@*/.all, 3.0/*@END_MENU_TOKEN@*/)
                        .frame(width: 180.0, height: 120.0)
                        .background(Color.black)
                    VStack{
                        HStack{
                            Text("March, 19 2021")
                                .padding(.leading, 5.0)
                                .font(.system(size: 12))
                                .foregroundColor(.red)
                            Text("Pokémon TV")
                                .font(.system(size: 15))
                                .foregroundColor(Color.gray)
                        }
                        HStack{
                            Text("Discover new Pokémon !!!")
                                .padding(.leading, 5.0)
                                .font(.system(size: 18))
                        }
                        .padding(.top, 5.0)
                        HStack{
                            NavigationLink(
                                destination: Image("898"),
                                label: {
                                    Text("More Detail")
                                        .padding(5.0)
                                        .frame(width: 150.0, height: 30.0)
                                        .background(Color.blue)
                                        .foregroundColor(.white)
                                        .cornerRadius(10)
                                })
                            Image(systemName: "heart.circle")
                                .resizable()
                                .frame(width: 25.0, height: 25.0)
                                .foregroundColor(.red)
                        }
                        .padding(.top, 5.0)
                    }
                    
                }
                HStack{
                    Image("pokemongo")
                        .resizable()
                        .padding(/*@START_MENU_TOKEN@*/.all, 3.0/*@END_MENU_TOKEN@*/)
                        .frame(width: 180.0, height: 120.0)
                        .background(Color.black)
                    VStack{
                        HStack{
                            Text("March, 15 2021")
                                .padding(.leading, 5.0)
                                .font(.system(size: 12))
                                .foregroundColor(.red)
                            Text("Pokémon GO")
                                .font(.system(size: 15))
                                .foregroundColor(Color.gray)
                        }
                        HStack{
                            Text("Forest for Wild Pokémon  ")
                                .padding(.leading, 5.0)
                                .font(.system(size: 18))
                        }
                        .padding(.top, 5.0)
                        HStack{
                            NavigationLink(
                                destination: Image("pokemongo"),
                                label: {
                                    Text("More Detail")
                                        .padding(5.0)
                                        .frame(width: 150.0, height: 30.0)
                                        .background(Color.blue)
                                        .foregroundColor(.white)
                                        .cornerRadius(10)
                                })
                            Image(systemName: "heart.circle")
                                .resizable()
                                .frame(width: 25.0, height: 25.0)
                                .foregroundColor(.red)
                        }
                        .padding(.top, 5.0)
                    }
                    
                    
                }
                HStack{
                    Image("starterPk")
                        .resizable()
                        .padding(/*@START_MENU_TOKEN@*/.all, 3.0/*@END_MENU_TOKEN@*/)
                        .frame(width: 180.0, height: 120.0)
                        .background(Color.black)
                    VStack{
                        HStack{
                            Text("March, 10 2021")
                                .padding(.leading, 5.0)
                                .font(.system(size: 12))
                                .foregroundColor(.red)
                            Text("Pokémon Trainer")
                                .font(.system(size: 15))
                                .foregroundColor(Color.gray)
                        }
                        HStack{
                            Text("Countdown 3 days For new Pokemon trainers")
                                .padding(.leading, 5.0)
                                .font(.system(size: 17))
                        }
                        .padding(.top, 5.0)
                        HStack{
                            NavigationLink(
                                destination: Image("starterPk2"),
                                label: {
                                    Text("More Detail")
                                        .padding(5.0)
                                        .frame(width: 150.0, height: 30.0)
                                        .background(Color.blue)
                                        .foregroundColor(.white)
                                        .cornerRadius(10)
                                })
                            Image(systemName: "heart.circle")
                                .resizable()
                                .frame(width: 25.0, height: 25.0)
                                .foregroundColor(.red)
                        }
                        .padding(.top, 5.0)
                    }
                    
                    
                }
                HStack{
                    Image("legendPk")
                        .resizable()
                        .padding(/*@START_MENU_TOKEN@*/.all, 3.0/*@END_MENU_TOKEN@*/)
                        .frame(width: 180.0, height: 120.0)
                        .background(Color.black)
                    VStack{
                        HStack{
                            Text("March, 2 2021")
                                .padding(.leading, 5.0)
                                .font(.system(size: 12))
                                .foregroundColor(.red)
                            Text("Pokémon TV")
                                .font(.system(size: 15))
                                .foregroundColor(Color.gray)
                        }
                        HStack{
                            Text("Nearly 100 years haven't seen the legendary Pokemon !!!")
                                .padding(.leading, 5.0)
                                .font(.system(size: 15))
                        }
                        .padding(.top, 5.0)
                        HStack{
                            NavigationLink(
                                destination: Image("legend"),
                                label: {
                                    Text("More Detail")
                                        .padding(5.0)
                                        .frame(width: 150.0, height: 30.0)
                                        .background(Color.blue)
                                        .foregroundColor(.white)
                                        .cornerRadius(10)
                                })
                            Image(systemName: "heart.circle")
                                .resizable()
                                .frame(width: 25.0, height: 25.0)
                                .foregroundColor(.red)
                        }
                        .padding(.top, 5.0)
                    }
                    
                    
                }
            }
            .navigationTitle("News")
            .toolbar {
                ToolbarItemGroup(placement: .navigationBarTrailing) {
                    VStack{
                        HStack{
                            NavigationLink(
                                destination: Home(),
                                label: {
                                    Image(systemName: "house")
                                        .resizable()
                                        .frame(width: 25.0, height: 20.0)
                                        .foregroundColor(.black)
                                        .padding()
                                })
                            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                                Image(systemName: "book")
                                    .resizable()
                                    .frame(width: 25, height: 20)
                                    .foregroundColor(.blue)
                                    .padding()
                            })
                                .padding(.leading, 2.5)
                            
                            NavigationLink(
                                destination: Text("This page is unavailable"),
                                label: {
                                    Image("icon_pokeball")
                                        .resizable()
                                        .frame(width: 40.0, height: 40.0)
                                        .padding()
                                })
                            .padding(.horizontal, 5.0)
                            NavigationLink(
                                destination: Text("This page is unavailable"),
                                label: {
                                    Image(systemName: "envelope")
                                        .resizable()
                                        .frame(width: 25.0, height: 20.0)
                                        .foregroundColor(.black)
                                        .padding()
                                })
                            .padding(.trailing, 2.5)
                            NavigationLink(
                                destination: Text("This page is unavailable"),
                                label: {
                                    Image(systemName: "person")
                                        .resizable()
                                        .frame(width: 25.0, height: 20.0)
                                        .foregroundColor(.black)
                                        .padding()
                                })
                        }
                    }
                    .padding(.bottom, 5.0)
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
