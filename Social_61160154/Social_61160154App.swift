//
//  Social_61160154App.swift
//  Social_61160154
//
//  Created by informatics on 3/17/21.
//

import SwiftUI

@main
struct Social_61160154App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
